var expect = require("chai").expect;
var request = require("request");

describe("Verify Home Page", function () {
  it("HomePage Verification", async function () {
    request("http://docker:5000", function (error, response, body) {
      console.log(body);
      expect(body).contains("Church Management System");
      expect(response.statusCode).to.equal(200);
      //   done();
    });
  });
});

describe("Verify Admin Board", function () {
  it("AdminBoard Verification", async function () {
    request("http://docker:5000/adminboard", function (
      error,
      response,
      body
    ) {
      expect(body).contains("Add Group");
      expect(body).contains("Register Member");
      expect(response.statusCode).to.equal(200);
      //   done();
    });
  });
});
